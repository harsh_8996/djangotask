
---
## Django Task
---

## Start

1. Create **env.py** file in same directory of sample_env.py.
2. Copy data from sample_env and paste it to env file with your database credential.
3. Create virtualenv and install requirements.
4. Run migrate command.
    ```
    python3 manage.py migrate
    ```
5. Run following comman to run fixtures.
    ```
    python3 manage.py loaddata fixtues.json
    ```
6. Run server
    ```
    python3 manage.py runserver
    ```
7. Use login api to for admin login or any user.
8. Use **add user api** to add users.
9. Use **update self data api**  for updating self's data.
10. Use **update and delete data api** for updating and deleting any user data, but only admin can do   this.
11. Use **get all user api** to show all users of app

---