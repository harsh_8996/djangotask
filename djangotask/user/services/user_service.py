from rest_framework import status
from rest_framework.response import Response

from user.models.user import UserManager
from user.models import User
from user.serializers import UserSerializer, GetUserSerializer

user_manager = UserManager()


class UserService:

    def get_all_user_data(self):
        
        users = User.objects.filter()
        users_data = UserSerializer(users,many=True).data
        return users_data
        # return Response({'data': users_data, 'message': 'Success'}, status=status.HTTP_200_OK)

    def get_user_data(self, user_id):
        
        user = User.objects.get(id=user_id)
        user_data = UserSerializer(user).data
        return user_data

    def add_user_data(self,data):

        data = dict(data)

        print('data ' , data)
        user_data = {
            'role' : 'User',
            'name' : data['name'][0],
            'email' : data['email'][0],
            'password' : data['password'][0],
            'mobile_no' : data['mobile_no'][0],
            'city' : data['city'][0],
            'address' : data['address'][0],

        }
        user_serializer = UserSerializer(data=user_data)
        if user_serializer.is_valid():
            
            user_manager.create_user(
                user_data['email'],
                user_data['password'],
                user_data['role'],
                user_data['mobile_no'],
                **{
                    'city':user_data['city'],
                    'address':user_data['address'],
                    'name' : user_data['name']
                }
            )

            response = self.get_all_user_data()
            return response

        else:
            print('dsd ' ,user_serializer.errors )
            response = self.get_all_user_data()
            return response
            # return Response({'error': user_serializer.errors, 'message': 'Serializer Error'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def update_user_data(self, user_id, data):
        
        user = User.objects.get(id=user_id)
            
        data = dict(data)

        user_data = {
            'name' : data['name'][0],
            'mobile_no' : data['mobile_no'][0],
            'city' : data['city'][0],
            'address' : data['address'][0]
        }

        user_serializer = UserSerializer(data=user_data,partial=True)
        if user_serializer.is_valid():
            user.update(user_data)
            user_data = GetUserSerializer(user).data
            return user_data
            # return Response({'data': user_data, 'message': 'Success'}, status=status.HTTP_200_OK)

        else:
            user_data = GetUserSerializer(user).data
            return user_data
            # return Response({'error': user_serializer.errors, 'message': 'Serializer Error'}, status=status.HTTP_406_NOT_ACCEPTABLE)

    def delete_user(self, user_id):

        # try:
        user = User.objects.get(id=user_id)
             
        # except Exception as e:
            # return Response({'error': str(e), 'message': 'User Not Found'}, status=status.HTTP_404_NOT_FOUND)

        user.delete()
        # return Response({'message': 'Successfully Deleted'}, status=status.HTTP_200_OK)
