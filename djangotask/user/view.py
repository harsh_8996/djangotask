from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect

from user.services import UserService
user_object = UserService()

def index(request):
    if request.user.id:
        return redirect('home')
    else:
        return render(request, 'index.html')

@login_required(login_url='/login')
def home(request):
    user_data = user_object.get_all_user_data()
    return render(request, 'home.html',{'user_data': user_data})

def login_view(request):

    if request.method == 'POST':
        username = request.POST['email']
        raw_password = request.POST['password']
        user = authenticate(username=username, password=raw_password)
        if not user:
            return render(request,'login.html',{'error':'Please Enter Valid Email & Password'})
        login(request, user)
        return redirect('home')
    else:
        return render(request,'login.html')

@login_required(login_url='/login')
def logout_view(request):

    logout(request)
    return redirect('index')

@login_required(login_url='/login')
def add_user_view(request):

    if request.user.role!='Admin':
        return render(request,'permission.html')

    if request.method == 'POST':
        data = user_object.add_user_data(request.POST)
        return redirect('home')
    else:
        return render(request,'add_user.html')

@login_required(login_url='/login')
def edit_user_view(request,id):

    if not (request.user.role =='Admin' or request.user.id==id):
        return render(request,'permission.html')

    try:
        if request.method == 'POST':
            data = user_object.update_user_data(id, request.POST)
            return redirect('home')
        else:
            user_data = user_object.get_user_data(id)
            return render(request,'edit_user.html',{'data':user_data})
    except Exception as e:
        return render(request,'error.html',{'error':str(e)})

@login_required(login_url='/login')
def delete_user_view(request, id):

    if request.user.role!='Admin':
        return render(request,'permission.html')

    try:
        user_object.delete_user(id)
        return redirect('home')
    except Exception as e:
        return render(request,'error.html',{'error':str(e)})