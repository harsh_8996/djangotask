from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class UserManager(BaseUserManager):

    def _create_user(self, email, password, role, mobile_no, **kwargs):

        if not email:
            raise ValueError('The given email must be set')
            
        user = User.objects.create(
            role = role,
            mobile_no = mobile_no,
            email = self.normalize_email(email),
            name = kwargs.get('name'),
            city = kwargs.get('city'),
            address = kwargs.get('address')

        )
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, email, password, role, mobile_no, **kwargs):
        return self._create_user(email, password, role, mobile_no,**kwargs)

    def create_superuser(self, email, password, role, mobile_no,**kwargs):
        if role != 'Admin':
            raise ValueError('Unauthorized')

        return self._create_user(email, password, role,  mobile_no,**kwargs)


class User(AbstractBaseUser):
    
    ROLE_CHOICES = (
        ('Admin', 'Admin'),
        ('User', 'User')
    )

    name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=255)

    email = models.EmailField(max_length=255,unique=True)
    password = models.CharField(max_length=255)
    
    mobile_no_regex = RegexValidator(regex=r'^\d{10}$', message='Phone number must contains 10 digits only')
    mobile_no = models.CharField(validators=[mobile_no_regex], max_length=10)

    role = models.CharField(choices=ROLE_CHOICES, max_length=10)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['role','password','mobile_no','name','city','address']

    class Meta:
        db_table = 'user'
        ordering = ('id',)
        
    def __str__(self):
        return self.email

    @property
    def is_staff(self):
        return True

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True
    
    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def update(self, updated_data):

        self.mobile_no = updated_data.get('mobile_no', self.mobile_no)
        self.name = updated_data.get('name', self.name)
        self.city = updated_data.get('city', self.city)
        self.address = updated_data.get('address', self.address)
        self.save()