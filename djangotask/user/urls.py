from django.conf.urls import url
from user.views import UserView, UserDetailView, UserListView

urlpatterns = [
    url(r'^$', UserView.as_view()),
    url(r'^(?P<id>\d+)/$', UserListView.as_view()),
    url(r'^details/$', UserDetailView.as_view()),
]