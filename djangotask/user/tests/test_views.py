
from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from user.models.user import User, UserManager
from user.services import UserService

client = Client()
user_object = UserManager()
user_service = UserService()

class LoginTest(TestCase):

	def setUp(self):

		kwargs = {
			'name' : 'abc',
			'city' : 'Goa',
			'address' : 'Goa'
		}
		return User.objects.create_user('abc@gmail.com','abc','User','9099499731',**kwargs)

	def test_login(self):
		response = client.post('/login/', {
			'email' : 'abc@gmail.com',
			'password' : 'abc'
		}, follow=True)
		self.assertEqual(str(response.context['user'].name), 'abc')


class UserDetailListTest(TestCase):

	@classmethod
	def setUpTestData(cls):

		kwargs = {
			'name' : 'abc',
			'city' : 'Goa',
			'address' : 'Goa'
		}
		return User.objects.create_user('abc@gmail.com','abc','User','9099499731',**kwargs)

	def test_home_url_accessible_by_name(self):
		login = client.login(username='abc@gmail.com', password='abc')
		self.assertEqual(login, True)
		response = client.get(reverse('index'))
		self.assertRedirects(response, '/home/')

	def test_home_uses_correct_template(self):
		login = client.login(username='abc@gmail.com', password='abc')
		self.assertEqual(login, True)
		response = client.get(reverse('home'))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'home.html')

	def test_user_name_when_login(self):
		login = client.login(username='abc@gmail.com', password='abc')
		self.assertEqual(login, True)
		response = client.get(reverse('home'))
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.context['user'].name,'abc')

	def test_redirect_login_if_logged(self):
		response = client.get(reverse('home'))
		self.assertEqual(response.url, '/login?next=/home/')


class UserTestCase(TestCase):

	def create_user(self):
		kwargs = {
			'name' : 'abc',
			'city' : 'Goa',
			'address' : 'Goa'
		}
		return user_object.create_user('abc@gmail.com','abc','User','9099499731',**kwargs)

	def test_add_user_data(self):
		before_no_of_users = User.objects.filter().count()
		user = self.create_user()
		users = User.objects.filter()
		self.assertTrue(isinstance(user, User))	
		self.assertEqual(len(users), before_no_of_users+1)

	def test_update_user_data(self):
		user = self.create_user()
		data = {
			'city' : 'Mumbai'
		}
		user.update(data)
		self.assertEqual(user.city, data['city'])

	def test_delete_user_data(self):
		before_no_of_users =User.objects.filter().count()
		user = self.create_user()
		user.delete()
		users = User.objects.filter().count()
		self.assertEqual(users, before_no_of_users)



