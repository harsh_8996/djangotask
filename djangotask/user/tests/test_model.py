from django.test import TestCase

from user.models.user import User, UserManager
user_object = UserManager()


class UserModelTest(TestCase):

	@classmethod
	def setUpTestData(cls):

		kwargs = {
			'name' : 'abc',
			'city' : 'Goa',
			'address' : 'Goa'
		}
		return User.objects.create_user('abc@gmail.com','abc','User','9099499731',**kwargs)

	def test_mobile_no_label(self):
		user = User.objects.get(id=1)
		field_label = user._meta.get_field('mobile_no').verbose_name
		self.assertEquals(field_label, 'mobile no')

	def test_object_name_is_email(self):
		user = User.objects.get(id=1)
		expected_object_name = user.email
		self.assertEquals(expected_object_name, str(user))
