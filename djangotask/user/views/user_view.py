from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from user.serializers import GetUserSerializer
from user.models.user import UserManager
from user.services import UserService

from djangotask.permission_classes import IsAdminOrNot

user_manager = UserManager()
user_object = UserService()


class UserView(APIView):

    permission_classes = (IsAdminOrNot,)

    def post(self, request):

        response = user_object.add_user_data(request.data)
        return response

class UserDetailView(APIView):

    def get(self, request):
        
        response = user_object.get_all_user_data()
        return response

    def put(self, request):

        response = user_object.update_user_data(request.user.id, request.data)
        return response


class UserListView(APIView):

    permission_classes = (IsAdminOrNot,)

    def put(self, request, id):
        
        response = user_object.update_user_data(id, request.data)
        return response

    def delete(self, request, id):

        response = user_object.delete_user(id)
        return response