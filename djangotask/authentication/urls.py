from django.conf.urls import url
from authentication.views import *

urlpatterns = [
    url(r'^login/$', LoginView.as_view()),
    url(r'^logout/$', LogoutView.as_view())    
]
