from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from oauth2_provider.views.mixins import OAuthLibMixin 
from oauth2_provider.settings import oauth2_settings


class LogoutView(OAuthLibMixin, APIView):

    server_class = oauth2_settings.OAUTH2_SERVER_CLASS
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS

    def post(self, request):

        response = self.validator_class.revoke_token(self,
        token = request.auth.token,
        token_type_hint = "access_token",
        request = request
        )   

        return Response({'message': 'Logout Successfully'}, status=status.HTTP_200_OK)
