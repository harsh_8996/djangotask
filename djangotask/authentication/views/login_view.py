import json

from django.contrib.auth import authenticate

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from oauth2_provider.views.mixins import OAuthLibMixin 
from oauth2_provider.settings import oauth2_settings

from user.models import User


class LoginView(OAuthLibMixin, APIView):

    permission_classes = (AllowAny,)

    server_class = oauth2_settings.OAUTH2_SERVER_CLASS
    validator_class = oauth2_settings.OAUTH2_VALIDATOR_CLASS
    oauthlib_backend_class = oauth2_settings.OAUTH2_BACKEND_CLASS

    def post(self, request):

        try:
            user = User.objects.get(email=request.data['username'])
        except Exception as e:
            return Response({'message': 'User Not Registered'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        user = authenticate(username=request.data['username'], password=request.data['password'])

        if user:
            uri, headers, body, status_code = self.create_token_response(request)

            if status_code != 200:
                return Response({'error': body,'message': 'Error In Token Generation'}, status=status.HTTP_406_NOT_ACCEPTABLE)

            data = {
                'token' : json.loads(body)['access_token']
            }
            return Response({'data':data, 'message': 'Success'}, status=status.HTTP_200_OK)

        else:
            return Response({'message': 'Please Enter Valid Password'}, status=status.HTTP_406_NOT_ACCEPTABLE)
