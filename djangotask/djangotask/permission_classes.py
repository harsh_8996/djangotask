from rest_framework import permissions

class IsAdminOrNot(permissions.BasePermission):

    def has_permission(self, request, view):

        try:
            if request.user.role == 'Admin':
                return True
            else:
                return False

        except Exception as e:
            return False

